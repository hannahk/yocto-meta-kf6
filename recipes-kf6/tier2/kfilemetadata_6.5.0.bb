# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kfilemetadata-6.5.0.tar.xz"
SRC_URI[sha256sum] = "574419823d7fe389dfc6bc141b0a9151fdada6715b985c8269293c0c04fdc0f4"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/syndication-6.5.0.tar.xz"
SRC_URI[sha256sum] = "cdafc51c9271b00150b8d63bf250d6d040f7bc8c56907af61dc59a44c96c674e"


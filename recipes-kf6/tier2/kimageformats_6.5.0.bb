# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kimageformats-6.5.0.tar.xz"
SRC_URI[sha256sum] = "c64ab736477264f8a0ce4418f0be629ad0f17a078161b2773700d3b96ca75022"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kcrash-6.5.0.tar.xz"
SRC_URI[sha256sum] = "870c6ce15132cc3040bc593447125ed3c256b698ba233b758430f4e725319bf3"


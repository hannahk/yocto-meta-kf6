# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kauth-6.5.0.tar.xz"
SRC_URI[sha256sum] = "1af517a509da1c5ddb4420d7430894b751562e07cc4c134abe0dcb606358d394"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kpackage-6.5.0.tar.xz"
SRC_URI[sha256sum] = "cf3452c1719112047f9a3bd00ab2e1e59ba7b6b4fe620a4353885308508db773"


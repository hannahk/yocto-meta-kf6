# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kcompletion-6.5.0.tar.xz"
SRC_URI[sha256sum] = "778af80e5015f49ce1e1dde6180bf51167a1f5dfb12d07c73216b5fa804eedf9"


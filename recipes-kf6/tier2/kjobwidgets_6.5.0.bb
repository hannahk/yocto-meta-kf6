# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kjobwidgets-6.5.0.tar.xz"
SRC_URI[sha256sum] = "67c5dab1191ae6830d452751767e94991b34feaf4228f18ab042c2c120910ad8"


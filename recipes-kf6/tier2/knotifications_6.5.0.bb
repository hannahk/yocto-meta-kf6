# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/knotifications-6.5.0.tar.xz"
SRC_URI[sha256sum] = "3d73dc682176138cba995b6954eeafdd4507097313f1b2102a4d5ed905a3eee6"


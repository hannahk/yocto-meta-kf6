# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kcolorscheme-6.5.0.tar.xz"
SRC_URI[sha256sum] = "323b55dd37dc408ccc158df2ce5c8a46b628f9355d2a77916e4565afce90b42b"


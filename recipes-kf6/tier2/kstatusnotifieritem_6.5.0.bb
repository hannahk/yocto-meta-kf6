# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kstatusnotifieritem-6.5.0.tar.xz"
SRC_URI[sha256sum] = "5def5e1a862d85d0f325c4f1973967bcf8fa97353fe1d361a1cafb0670198403"


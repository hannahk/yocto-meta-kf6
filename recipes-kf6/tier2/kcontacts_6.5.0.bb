# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kcontacts-6.5.0.tar.xz"
SRC_URI[sha256sum] = "b711e098469a5821044bf99bd74d0a16b804731a347cf53609a4bd1b5fa5fdc4"


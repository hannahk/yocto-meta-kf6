# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kunitconversion-6.5.0.tar.xz"
SRC_URI[sha256sum] = "c7d521423c7443d305803e2f606e8dff58fa9e1c7c73b09bce8dd3862e992fe4"


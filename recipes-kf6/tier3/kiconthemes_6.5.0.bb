# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kiconthemes-6.5.0.tar.xz"
SRC_URI[sha256sum] = "cdc4c5788e0b3f88f25aa474d51d43496e4c742777f88025ef2fa606f2721331"


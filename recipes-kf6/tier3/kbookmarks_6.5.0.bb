# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kbookmarks-6.5.0.tar.xz"
SRC_URI[sha256sum] = "97dabceae5b0eac1107c49c50d1d7d9a7d26b9246c9ab53d123990525c55fbec"


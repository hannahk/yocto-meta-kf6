# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/knotifyconfig-6.5.0.tar.xz"
SRC_URI[sha256sum] = "fb2ad6bffb54e6a05fab4dc871897beaba03a46d42fa290b56ee1fcbb2ee0594"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kio-6.5.0.tar.xz"
SRC_URI[sha256sum] = "9c8bf83534577a322d4633d241d9770bc8ba8a45624e2f041e1b8dbdbc198a13"


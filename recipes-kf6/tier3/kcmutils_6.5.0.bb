# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kcmutils-6.5.0.tar.xz"
SRC_URI[sha256sum] = "eb8474ec5ae620e361e6ef971e3ec14ac6807be2df8e02d27e7d8ae9badca993"


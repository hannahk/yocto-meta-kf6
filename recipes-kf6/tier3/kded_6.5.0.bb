# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kded-6.5.0.tar.xz"
SRC_URI[sha256sum] = "292ca941acc69c443f47895ea784493b08fd573afe0f4e99f5f42b8a029954b0"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/qqc2-desktop-style-6.5.0.tar.xz"
SRC_URI[sha256sum] = "888638775a4c8bb7f80e10e878fd923880c75f2aa6e3878d52c12a77f3da9321"


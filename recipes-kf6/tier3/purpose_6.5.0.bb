# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/purpose-6.5.0.tar.xz"
SRC_URI[sha256sum] = "40f80a04a737f5b79d9980baabdca54c820456bb30add04479257291c62ac274"


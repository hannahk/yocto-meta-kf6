# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/krunner-6.5.0.tar.xz"
SRC_URI[sha256sum] = "5674afef23fe6747aaa8b06c405a53a4e40f77ba24535d2080d512602b036030"


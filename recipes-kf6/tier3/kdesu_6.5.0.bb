# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kdesu-6.5.0.tar.xz"
SRC_URI[sha256sum] = "03c08d0bbcf71ef122ebe3c97b109092ad9e8719d20d341d8510607bda9ab393"


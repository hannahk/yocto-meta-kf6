# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/ktextwidgets-6.5.0.tar.xz"
SRC_URI[sha256sum] = "a99df1c634831e9d01f704009c951378108334a4258ad5b64f60f55e55770212"


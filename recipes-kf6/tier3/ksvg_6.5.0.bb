# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/ksvg-6.5.0.tar.xz"
SRC_URI[sha256sum] = "ae2024bd4d7e6950c92b5924deced5d55b6101749d635799b24fedac60cb26cd"


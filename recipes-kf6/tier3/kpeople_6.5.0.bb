# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kpeople-6.5.0.tar.xz"
SRC_URI[sha256sum] = "102cd7ad3260b0d10bd2ec0330c839826ea324c5cd86580163d8364d3370a6c2"


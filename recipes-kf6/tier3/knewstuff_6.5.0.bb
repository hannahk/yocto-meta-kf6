# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/knewstuff-6.5.0.tar.xz"
SRC_URI[sha256sum] = "815589a660b9a53a1fc18268b95914636124b6f3f3193c9404e0959f8b738c79"


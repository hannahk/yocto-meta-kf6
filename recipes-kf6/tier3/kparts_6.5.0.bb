# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kparts-6.5.0.tar.xz"
SRC_URI[sha256sum] = "4eb5417b267f77c16c85482f919adfb5b7714a95239771c4aafbfeb90e4a371f"


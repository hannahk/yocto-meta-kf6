# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kdav-6.5.0.tar.xz"
SRC_URI[sha256sum] = "900fbb8b632d946cd35e826e64f50507346151f1b5fb40137890abc2896eefcf"


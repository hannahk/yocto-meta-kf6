# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kservice-6.5.0.tar.xz"
SRC_URI[sha256sum] = "48cdd204bc9fb9282c37b6bcc2364f7dbc124057547ddb6b1471988b464cf8bd"


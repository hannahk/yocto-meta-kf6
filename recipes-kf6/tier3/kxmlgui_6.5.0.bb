# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kxmlgui-6.5.0.tar.xz"
SRC_URI[sha256sum] = "75549dd54ae7b3e0bf01c6d82ebe2dcc797195ade223b6986dde9d688c5cd903"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kwallet-6.5.0.tar.xz"
SRC_URI[sha256sum] = "9eb9ef50a10319afdf8ddbab06bb76c05f43d8d4095483f2d8efed752d5d815a"


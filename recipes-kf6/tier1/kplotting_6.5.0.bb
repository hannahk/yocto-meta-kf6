# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kplotting-6.5.0.tar.xz"
SRC_URI[sha256sum] = "021697c4d42002fad49db0d283552a2b40e81f968763d87b3ad47ec3f580d943"


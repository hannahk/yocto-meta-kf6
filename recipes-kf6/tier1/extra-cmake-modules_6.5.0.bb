# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/extra-cmake-modules-6.5.0.tar.xz"
SRC_URI[sha256sum] = "8f3c2ca1e502990629f3b68507189fc0f912f3cab279b500dac91ee7031a49cf"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/karchive-6.5.0.tar.xz"
SRC_URI[sha256sum] = "e5530253c70de024926e1985154f9115f02af50c7d998a874a3175b404444e79"


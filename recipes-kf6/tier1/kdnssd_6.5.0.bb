# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kdnssd-6.5.0.tar.xz"
SRC_URI[sha256sum] = "37fd254c39b66fca1b52f898c045f322a0ea3177c927941979ccb7b9b98ebffd"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kuserfeedback-6.5.0.tar.xz"
SRC_URI[sha256sum] = "3348d2f29b92e655249b750fd77fb56bc4511ba3ba74399bd3fb2440821a292a"


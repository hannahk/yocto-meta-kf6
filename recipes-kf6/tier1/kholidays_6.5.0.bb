# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kholidays-6.5.0.tar.xz"
SRC_URI[sha256sum] = "84a03085ffafab06cd28a32154b0fe4694f58b37d21f1468818b5fec2c643dca"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kcodecs-6.5.0.tar.xz"
SRC_URI[sha256sum] = "e96c22f5470e39591acdbb01e85203e5d2f179c80b18c2a5991967e16af91e08"


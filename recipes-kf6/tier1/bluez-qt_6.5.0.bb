# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/bluez-qt-6.5.0.tar.xz"
SRC_URI[sha256sum] = "f0234766fc1af9410708d0886eef304175b09a5b99c48c0eb877a80fec6aa65e"


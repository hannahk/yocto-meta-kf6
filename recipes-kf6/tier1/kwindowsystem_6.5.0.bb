# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kwindowsystem-6.5.0.tar.xz"
SRC_URI[sha256sum] = "caae86e16bf26e91a722fdf8eb5888224f7c76ce870743da4a59bc700d873bc6"


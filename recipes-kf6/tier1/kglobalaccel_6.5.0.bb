# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kglobalaccel-6.5.0.tar.xz"
SRC_URI[sha256sum] = "883a1cf48fc4b8ce22ab9f143b6bdd546ac30fbe29c90d4035fb2adf38a339a4"


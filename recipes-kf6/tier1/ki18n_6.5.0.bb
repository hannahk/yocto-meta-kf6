# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/ki18n-6.5.0.tar.xz"
SRC_URI[sha256sum] = "7873d36a8ae452745d58100e7cda067eae632c787c1fc5a3c63a7072aacfa7fc"


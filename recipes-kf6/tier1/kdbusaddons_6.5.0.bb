# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kdbusaddons-6.5.0.tar.xz"
SRC_URI[sha256sum] = "afacf9ff5d7a2dd294520718508d33eadf3f63a957091d30a68a45ab1ef0a8fe"


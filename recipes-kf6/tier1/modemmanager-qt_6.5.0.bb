# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/modemmanager-qt-6.5.0.tar.xz"
SRC_URI[sha256sum] = "76c2b18c9c5c5d9c360282a031feaae966029322f19de050d91d39207cd5717b"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kirigami-6.5.0.tar.xz"
SRC_URI[sha256sum] = "43a73b161e1c85da3eadc63e7cc6c1b3c686aa56951b0d0e2df4a2cc1334759c"


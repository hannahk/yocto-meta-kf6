# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/ktexttemplate-6.5.0.tar.xz"
SRC_URI[sha256sum] = "295a3f87ff08af17f83496fd66b3f634917bcac6466c79be1f4cb6786ecb04c6"


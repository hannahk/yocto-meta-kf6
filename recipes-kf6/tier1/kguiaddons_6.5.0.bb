# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kguiaddons-6.5.0.tar.xz"
SRC_URI[sha256sum] = "7193fa930b85fa6e7fda3a85f1e52f362ecd3e110e80055d9084eeafaeac4807"


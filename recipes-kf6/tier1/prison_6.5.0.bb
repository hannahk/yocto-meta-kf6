# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/prison-6.5.0.tar.xz"
SRC_URI[sha256sum] = "bbc17dccfb71988af41e70110b6635323e17d8ed48c08f73808f9f220fb067ba"


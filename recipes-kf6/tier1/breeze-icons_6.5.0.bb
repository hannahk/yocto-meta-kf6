# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/breeze-icons-6.5.0.tar.xz"
SRC_URI[sha256sum] = "ca6e8faef84891750ebc240d0b99f42414e5f643678d5b1ae94bcbad551ab0c4"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/solid-6.5.0.tar.xz"
SRC_URI[sha256sum] = "e8237c6c9617bef4bf5fc74461bb7417ca57afe15d4f54878cfe8c806e706a5c"


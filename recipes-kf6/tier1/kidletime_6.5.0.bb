# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kidletime-6.5.0.tar.xz"
SRC_URI[sha256sum] = "b6dc7d6eadb642248000f165155a72d2dfab6c1a93e0130f8f83394a7628eaf6"


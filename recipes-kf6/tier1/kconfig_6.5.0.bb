# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kconfig-6.5.0.tar.xz"
SRC_URI[sha256sum] = "e48e5315d3491ddfb878abf124a6e14886a6317857fa63f411ecd720da8a5d13"


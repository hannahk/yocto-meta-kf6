# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/attica-6.5.0.tar.xz"
SRC_URI[sha256sum] = "6ce80618dc52a7a2c48a425617161ec46b7126d05ecb23076e655fde1d6010e6"


# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kquickcharts-6.5.0.tar.xz"
SRC_URI[sha256sum] = "c5a9a5cc206fdd2a86854284b081b16575f638bbb28b658065677b447d8c9002"


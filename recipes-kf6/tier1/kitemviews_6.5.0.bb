# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/kitemviews-6.5.0.tar.xz"
SRC_URI[sha256sum] = "07f3b3880597995a9bbc3c9bc47dba2bcd924ec503b423ed8566e7e805cbb69c"


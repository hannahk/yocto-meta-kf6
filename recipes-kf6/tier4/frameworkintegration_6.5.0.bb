# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

require ${PN}.inc
SRC_URI = "https://download.kde.org/stable/frameworks/6.5/frameworkintegration-6.5.0.tar.xz"
SRC_URI[sha256sum] = "c57b55742e602d2c85c9002158e88fe077d3a164c68ed50c77672a74e11a0df4"

